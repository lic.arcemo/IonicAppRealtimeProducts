import { Component, OnInit } from '@angular/core';
import io from 'socket.io-client';
var socket = io('http://5.189.137.34:8080');

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})

export class ProductsPage {

  public products = [];
  constructor() {

  }

  ngOnInit() {
    //Obtiene listado de productos
    socket.on('getProducts',(data)=> {
      this.products = data;
      // this.changeDetection.detectChanges();
   });

   //Escucha cuando un producto es agradado
   socket.on('addProduct',(data)=> {
     this.products.push(data);
     // this.changeDetection.detectChanges();
  });
  }


}
