import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import io from 'socket.io-client';
var socket = io('http://5.189.137.34:8080');


@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.scss'],
})

export class AddProductPage {

  public product = {
    id: 0
  };
  
  constructor( private router: Router ) {

  }

  ngOnInit() {

  }

  addProduct() {
    this.product.id = Math.floor((Math.random() * 100) + 1);
     socket.emit('newProduct',this.product);
    this.router.navigate(['/'])
  }


}
